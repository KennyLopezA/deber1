fun main() {
    val spainCurrency = CurrencyFactory.currencyForCountry(Country.Spain).code
    println("Moneda España: " + spainCurrency)

    val ecuadorCurrency = CurrencyFactory.currencyForCountry(Ecuador).code
    println("Moneda Ecuador: " + ecuadorCurrency)
}

sealed class Country {
    object Spain : Country()
}

object Ecuador: Country()

class Currency(
    val code: String
)

object CurrencyFactory {
    fun currencyForCountry(country: Country): Currency =
        when (country) {
            is Country.Spain -> Currency("EUR")
            is Ecuador -> Currency("USA")
        }
}